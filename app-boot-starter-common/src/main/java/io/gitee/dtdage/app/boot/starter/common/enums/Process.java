package io.gitee.dtdage.app.boot.starter.common.enums;

import io.gitee.dtdage.app.boot.starter.common.BaseEnum;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 流程标记
 *
 * @author WFT
 * @since 2024/4/14
 */
@Getter
@AllArgsConstructor
public enum Process implements BaseEnum<Integer> {

    /**
     * 新事件（待处理）
     */
    fresh(1),

    /**
     * 处决中
     */
    execute(2),

    /**
     * 处决结果：成功
     */
    success(3),

    /**
     * 处决结果：失败
     */
    fail(4),

    /**
     * 流程结束
     */
    done(5);

    private final Integer value;

}
