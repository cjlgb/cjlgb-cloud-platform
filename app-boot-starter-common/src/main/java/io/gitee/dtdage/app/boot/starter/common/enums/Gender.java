package io.gitee.dtdage.app.boot.starter.common.enums;

import io.gitee.dtdage.app.boot.starter.common.BaseEnum;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 性别标记
 *
 * @author WFT
 * @since 2024/4/14
 */
@Getter
@AllArgsConstructor
public enum Gender implements BaseEnum<Integer> {

    /**
     * 未知
     */
    unknown(0),

    /**
     * 男
     */
    man(1),

    /**
     * 女
     */
    woman(2);

    private final Integer value;

}

