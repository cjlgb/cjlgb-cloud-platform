package io.gitee.dtdage.app.boot.starter.common.utils.function;

/**
 * @author WFT
 * @since 2024/3/30
 */
@FunctionalInterface
public interface BiFunction<T, U, R> {

    /**
     * Applies this function to the given arguments.
     *
     * @param t the first function argument
     * @param u the second function argument
     * @return the function result
     * @throws Exception java.lang.Exception
     */
    R apply(T t, U u) throws Exception;

}
