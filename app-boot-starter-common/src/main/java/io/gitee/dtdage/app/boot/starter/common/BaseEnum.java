package io.gitee.dtdage.app.boot.starter.common;

import java.io.Serializable;

/**
 * @author WFT
 * @since 2024/4/14
 */
public interface BaseEnum<T extends Serializable> extends Serializable {

    /**
     * 获取值
     *
     * @return {@link Serializable}
     */
    T getValue();

}
