package io.gitee.dtdage.app.boot.starter.common;

import java.io.Serializable;

/**
 * @author WFT
 * @since 2024/4/14
 */
public interface BaseBean<T extends Serializable> extends Serializable {

    /**
     * 获取主键
     *
     * @return {@link Serializable}
     */
    T getId();

}
