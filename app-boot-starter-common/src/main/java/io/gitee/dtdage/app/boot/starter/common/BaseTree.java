package io.gitee.dtdage.app.boot.starter.common;

import java.io.Serializable;
import java.util.List;

/**
 * @author WFT
 * @since 2024/4/14
 */
public interface BaseTree<K extends Serializable, T extends BaseEntity<K>> extends BaseEntity<K> {

    /**
     * 获取父级编号
     *
     * @return {@link Serializable}
     */
    K getPid();

    /**
     * 获取排序值
     *
     * @return {@link Integer}
     */
    Integer getSort();

    /**
     * 设置子节点列表
     *
     * @param list {@link List}
     */
    void setChildren(List<T> list);

    /**
     * 获取子节点列表
     *
     * @return {@link List}
     */
    List<T> getChildren();

}
