package io.gitee.dtdage.app.boot.starter.common.utils.function;

/**
 * @author WFT
 * @since 2024/3/30
 */
@FunctionalInterface
public interface Function<T, R> {

    /**
     * Applies this function to the given argument.
     *
     * @param t the function argument
     * @return the function result
     * @throws Exception java.lang.Exception
     */
    R apply(T t) throws Exception;

}
