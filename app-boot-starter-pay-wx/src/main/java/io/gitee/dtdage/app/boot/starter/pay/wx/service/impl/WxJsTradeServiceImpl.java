package io.gitee.dtdage.app.boot.starter.pay.wx.service.impl;

import io.gitee.dtdage.app.boot.starter.pay.common.context.TradeBean;
import io.gitee.dtdage.app.boot.starter.pay.common.enums.Scene;
import io.gitee.dtdage.app.boot.starter.pay.wx.context.ConfigBean;
import io.gitee.dtdage.app.boot.starter.pay.wx.service.BaseTradeService;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.configurationprocessor.json.JSONObject;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;

/**
 * 微信公众号,小程序交易接口实现类
 *
 * @author WFT
 * @since 2024/4/9
 */
@Service
@RequiredArgsConstructor
public class WxJsTradeServiceImpl extends BaseTradeService {

    @Override
    public Scene getId() {
        return Scene.wx_jsapi;
    }

    /**
     * @noinspection SpellCheckingInspection, Duplicates
     */
    @Override
    public <T extends TradeBean> Object payment(T param) throws Exception {
        //  获取客户端配置
        ConfigBean configure = this.getConfigure(param.getClientId());
        //  请求体
        JSONObject data = new JSONObject()
                //  公众号,小程序编号
                .put("appid", configure.getAppId())
                //  商户号
                .put("mchid", configure.getMchId())
                //  订单描述
                .put("description", param.getTradeTitle())
                //  订单编号
                .put("out_trade_no", String.valueOf(param.getId()))
                //  支付回调
                .put("notify_url", String.format(configure.getPayNotifyPath(), configure.getMchId(), param.getId()))
                //  订单金额
                .put("amount", new JSONObject().put("total", param.getTradePrice()).put("currency", "CNY"))
                //  买家信息
                .put("payer", new JSONObject().put("openid", param.getOutPayerId()))
                //  结算信息,不开启分账
                .put("settle_info", new JSONObject().put("profit_sharing", false));
        //  接口地址
        String api = "/v3/pay/transactions/jsapi";
        //  调用接口
        return this.execute(api, data.toString(), configure, HttpMethod.POST, response -> this.callback(configure, response));
    }

}
