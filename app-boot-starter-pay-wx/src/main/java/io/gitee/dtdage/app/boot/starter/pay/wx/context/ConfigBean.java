package io.gitee.dtdage.app.boot.starter.pay.wx.context;

/**
 * 微信配置
 *
 * @author WFT
 * @since 2024/4/14
 */
public interface ConfigBean extends io.gitee.dtdage.app.boot.starter.pay.common.context.ConfigBean {

    /**
     * @return 公众号密钥
     */
    String getAppSecret();

    /**
     * @return 微信商户编号
     */
    String getMchId();

    /**
     * @return 微信商户密钥
     */
    String getMchSecret();

    /**
     * @return 微信商户API证书的存储路径
     */
    String getApiCertPath();

    /**
     * @return 微信商户私钥
     */
    String getPrivateKey();

    /**
     * @return 微信商户序列号
     */
    String getSerialNumber();

    /**
     * @return 退款通知API地址
     */
    String getRefNotifyPath();

    /**
     * @return 提现场景编号
     */
    String getExtSceneId();

    /**
     * @return 提现通知API地址
     */
    String getExtNotifyPath();

}
