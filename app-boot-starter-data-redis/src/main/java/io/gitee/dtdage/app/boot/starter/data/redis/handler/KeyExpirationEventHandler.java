package io.gitee.dtdage.app.boot.starter.data.redis.handler;

import io.gitee.dtdage.app.boot.starter.common.BaseBean;

/**
 * @author WFT
 * @since 2024/4/14
 */
public interface KeyExpirationEventHandler extends BaseBean<String> {

    /**
     * 过期事件接口
     *
     * @param args Key相关参数
     */
    void onApplicationEvent(String[] args);

}
