package io.gitee.dtdage.app.boot.starter.data.mybatis.context;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.gitee.dtdage.app.boot.starter.common.BaseEntity;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotNull;

/**
 * 分页对象
 *
 * @author WFT
 * @since 2024/4/14
 */
@Getter
@Setter
@NoArgsConstructor
public class PageBean<T extends BaseEntity> extends Page<T> implements IPage<T> {

    /**
     * 查询条件
     */
    @NotNull(message = "查询条件不能为空!")
    private T params;

    /**
     * 排序字段
     */
    private String orderBy = "id";

    private Boolean isAsc = true;

}
