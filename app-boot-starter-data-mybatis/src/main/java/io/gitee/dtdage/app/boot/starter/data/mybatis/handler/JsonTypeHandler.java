package io.gitee.dtdage.app.boot.starter.data.mybatis.handler;

import com.baomidou.mybatisplus.extension.handlers.AbstractJsonTypeHandler;
import io.gitee.dtdage.app.boot.starter.common.utils.JsonUtil;
import lombok.RequiredArgsConstructor;

/**
 * Json类型处理器
 *
 * @author WFT
 * @since 2024/4/14
 */
@RequiredArgsConstructor
public class JsonTypeHandler<T> extends AbstractJsonTypeHandler<T> {

    private final Class<T> clazz;

    @Override
    protected T parse(String json) {
        return JsonUtil.getInstance().toBean(json, this.clazz);
    }

    @Override
    protected String toJson(T obj) {
        return JsonUtil.getInstance().toJson(obj);
    }

}
