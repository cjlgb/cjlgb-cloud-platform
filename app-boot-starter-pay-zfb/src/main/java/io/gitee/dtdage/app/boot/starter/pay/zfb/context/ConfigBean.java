package io.gitee.dtdage.app.boot.starter.pay.zfb.context;

/**
 * 支付宝配置
 *
 * @author WFT
 * @since 2024/4/14
 */
public interface ConfigBean extends io.gitee.dtdage.app.boot.starter.pay.common.context.ConfigBean {

    /**
     * @return 应用私钥
     */
    String getPrivateKey();

    /**
     * @return 应用公钥
     */
    String getPublicKey();

    /**
     * @return 支付中断后跳转的地址
     */
    String getCancelPath();

    /**
     * @return 支付成功后跳转的页面
     */
    String getSuccessPath();

    /**
     * @return 支付宝根证书存储路径
     */
    String getRootCertPath();

    /**
     * @return 应用公钥证书存储路径
     */
    String getAppCertPath();

    /**
     * @return 支付宝公钥证书存储路径
     */
    String getZfbCertPath();

    /**
     * @return 供应商编号
     */
    String getSupplierId();

}
