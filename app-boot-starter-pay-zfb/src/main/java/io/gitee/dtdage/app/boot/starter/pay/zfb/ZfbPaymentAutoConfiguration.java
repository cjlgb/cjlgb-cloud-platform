package io.gitee.dtdage.app.boot.starter.pay.zfb;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * @author WFT
 * @since 2024/4/14
 */
@Configuration
@ComponentScan
public class ZfbPaymentAutoConfiguration {



}
