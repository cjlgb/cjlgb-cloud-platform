package io.gitee.dtdage.app.boot.starter.pay.common.context;

import io.gitee.dtdage.app.boot.starter.common.context.BeanFactory;
import io.gitee.dtdage.app.boot.starter.pay.common.enums.Scene;
import io.gitee.dtdage.app.boot.starter.pay.common.service.BaseTradeService;
import org.springframework.stereotype.Component;

/**
 * 交易工厂
 *
 * @author WFT
 * @since 2024/4/14
 */
@Component
public class TradeFactory extends BeanFactory<Scene, BaseTradeService<?>> {

    @Override
    protected IllegalArgumentException illegalArgumentException() {
        return new IllegalArgumentException("未实现此交易接口");
    }

}

