package io.gitee.dtdage.app.boot.starter.pay.common.service;

import io.gitee.dtdage.app.boot.starter.common.BaseBean;
import io.gitee.dtdage.app.boot.starter.pay.common.context.ConfigBean;
import io.gitee.dtdage.app.boot.starter.pay.common.context.ExtractBean;
import io.gitee.dtdage.app.boot.starter.pay.common.context.RefundBean;
import io.gitee.dtdage.app.boot.starter.pay.common.context.TradeBean;
import io.gitee.dtdage.app.boot.starter.pay.common.enums.Scene;

/**
 * 交易接口
 *
 * @author WFT
 * @since 2024/4/14
 */
public abstract class BaseTradeService<C extends ConfigBean> implements BaseBean<Scene> {

    /**
     * 获取客户端配置
     *
     * @param clientId 客户端编号
     * @return {@link ConfigBean}
     */
    protected abstract C getConfigure(String clientId);

    /**
     * 发起支付
     *
     * @param param 交易参数
     * @param <T>   {@link TradeBean}
     * @return 客户端唤起支付所需的参数
     * @throws Exception 异常
     */
    public abstract <T extends TradeBean> Object payment(T param) throws Exception;

    /**
     * 发起提现
     *
     * @param param 提现参数
     * @param <T>   {@link ExtractBean}
     * @throws Exception 异常
     */
    public abstract <T extends ExtractBean> void extract(T param) throws Exception;

    /**
     * 发起退款
     *
     * @param param 退款参数
     * @param <T>   {@link RefundBean}
     * @throws Exception 异常
     */
    public abstract <T extends RefundBean> void refund(T param) throws Exception;

}
