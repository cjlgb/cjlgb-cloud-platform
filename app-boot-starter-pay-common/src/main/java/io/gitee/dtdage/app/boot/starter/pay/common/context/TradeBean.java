package io.gitee.dtdage.app.boot.starter.pay.common.context;

import io.gitee.dtdage.app.boot.starter.common.BaseEntity;

/**
 * 交易参数接口
 *
 * @author WFT
 * @since 2024/4/14
 */
public interface TradeBean extends BaseEntity<Long> {

    /**
     * @return 交易名称
     */
    String getTradeTitle();

    /**
     * @return 交易金额
     */
    Integer getTradePrice();

    /**
     * @return 付款人编号
     */
    String getOutPayerId();

    /**
     * @return 客户端编号
     */
    String getClientId();

    /**
     * @return 客户端IP
     */
    String getClientIp();

}