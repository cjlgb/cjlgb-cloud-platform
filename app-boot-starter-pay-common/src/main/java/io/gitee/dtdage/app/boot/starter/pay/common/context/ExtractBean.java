package io.gitee.dtdage.app.boot.starter.pay.common.context;

import io.gitee.dtdage.app.boot.starter.common.BaseEntity;

/**
 * 提现参数接口
 *
 * @author WFT
 * @since 2024/4/14
 */
public interface ExtractBean extends BaseEntity<Long> {

    /**
     * @return 交易名称
     */
    String getExtractTitle();

    /**
     * @return 交易金额, 单位：分
     */
    Integer getExtractPrice();

    /**
     * @return 收款人编号
     */
    String getOutPayeeId();

    /**
     * @return 收款人名称
     */
    String getOutPayeeName();

    /**
     * @return 客户端编号
     */
    String getClientId();


}
