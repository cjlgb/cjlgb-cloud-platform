package io.gitee.dtdage.app.boot.starter.pay.common.service;

import io.gitee.dtdage.app.boot.starter.pay.common.context.ConfigBean;

/**
 * 配置业务接口
 *
 * @author WFT
 * @since 2024/4/14
 */
public interface ConfigureService<T extends ConfigBean> {

    /**
     * 获取客户端配置
     *
     * @param clientId 客户端编号
     * @return {@link ConfigBean}
     */
    T getConfigure(String clientId);

}
