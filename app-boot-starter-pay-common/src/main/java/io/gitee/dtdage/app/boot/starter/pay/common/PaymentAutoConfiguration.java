package io.gitee.dtdage.app.boot.starter.pay.common;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * 支付模块自动配置类
 *
 * @author WFT
 * @since 2024/4/9
 */
@Configuration
@ComponentScan
public class PaymentAutoConfiguration {



}
