package io.gitee.dtdage.app.boot.starter.web.common.exception.handler;

import io.gitee.dtdage.app.boot.starter.web.common.context.Response;
import io.gitee.dtdage.app.boot.starter.web.common.exception.BusinessException;
import io.gitee.dtdage.app.boot.starter.web.common.handler.ExceptionHandler;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * 业务异常处理器
 *
 * @author WFT
 * @since 2024/3/30
 */
@Slf4j
@Component
public class BusinessExceptionHandler implements ExceptionHandler<Class<BusinessException>> {

    @Override
    public <E extends Throwable> Response<?> resolve(E exception) {
        BusinessException e = (BusinessException) exception;
        return new Response<>(e.getCode(), e.getMessage());
    }

    @Override
    public Class<BusinessException> getId() {
        return BusinessException.class;
    }

}
