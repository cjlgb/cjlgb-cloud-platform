package io.gitee.dtdage.app.boot.starter.web.common.exception.handler;

import org.springframework.stereotype.Component;
import org.springframework.web.bind.MissingRequestHeaderException;

/**
 * @author WFT
 * @since 2024/3/30
 */
@Component
public class MissingRequestHeaderExceptionHandler extends BaseParameterExceptionHandler<Class<MissingRequestHeaderException>> {

    @Override
    public Class<MissingRequestHeaderException> getId() {
        return MissingRequestHeaderException.class;
    }

}