package io.gitee.dtdage.app.boot.starter.web.common.context;

import java.lang.annotation.*;

/**
 * 忽略响应体注解
 *
 * @author WFT
 * @since 2024/4/15
 */
@Inherited
@Documented
@Target(value = ElementType.METHOD)
@Retention(value = RetentionPolicy.RUNTIME)
public @interface Ignore {

}
