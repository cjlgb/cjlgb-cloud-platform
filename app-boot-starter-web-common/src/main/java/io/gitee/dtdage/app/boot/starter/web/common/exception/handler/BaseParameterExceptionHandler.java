package io.gitee.dtdage.app.boot.starter.web.common.exception.handler;

import io.gitee.dtdage.app.boot.starter.web.common.context.HttpStatus;
import io.gitee.dtdage.app.boot.starter.web.common.context.Response;
import io.gitee.dtdage.app.boot.starter.web.common.handler.ExceptionHandler;

/**
 * 参数异常处理器
 *
 * @author WFT
 * @since 2024/3/30
 */
public abstract class BaseParameterExceptionHandler<T extends Class<? extends Throwable>> implements ExceptionHandler<T> {

    @Override
    public <E extends Throwable> Response<?> resolve(E exception) {
        return new Response<>(HttpStatus.BAD_REQUEST, exception.getMessage());
    }

}
