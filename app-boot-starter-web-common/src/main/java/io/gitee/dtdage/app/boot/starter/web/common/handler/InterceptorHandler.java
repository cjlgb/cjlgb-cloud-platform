package io.gitee.dtdage.app.boot.starter.web.common.handler;

import org.springframework.web.servlet.HandlerInterceptor;

import java.util.List;

/**
 * 拦截器处理器接口
 *
 * @author WFT
 * @since 2024/4/14
 */
public interface InterceptorHandler extends HandlerInterceptor {

    /**
     * 路径匹配规则
     *
     * @return {@link List}
     */
    List<String> getPathPatterns();

}
