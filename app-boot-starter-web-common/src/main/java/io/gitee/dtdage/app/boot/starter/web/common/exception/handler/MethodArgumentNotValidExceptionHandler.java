package io.gitee.dtdage.app.boot.starter.web.common.exception.handler;

import io.gitee.dtdage.app.boot.starter.web.common.context.HttpStatus;
import io.gitee.dtdage.app.boot.starter.web.common.context.Response;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.MethodArgumentNotValidException;

/**
 * @author WFT
 * @since 2024/3/30
 */
@Component
public class MethodArgumentNotValidExceptionHandler extends BaseParameterExceptionHandler<Class<MethodArgumentNotValidException>> {

    @Override
    public Class<MethodArgumentNotValidException> getId() {
        return MethodArgumentNotValidException.class;
    }

    @Override
    public <E extends Throwable> Response<?> resolve(E exception) {
        MethodArgumentNotValidException e = (MethodArgumentNotValidException) exception;
        return new Response<>(HttpStatus.BAD_REQUEST, e.getBindingResult().getAllErrors().get(0).getDefaultMessage());
    }
}