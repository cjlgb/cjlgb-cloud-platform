package io.gitee.dtdage.app.boot.starter.web.common.exception.handler;

import io.gitee.dtdage.app.boot.starter.web.common.context.HttpStatus;
import io.gitee.dtdage.app.boot.starter.web.common.context.Response;
import io.gitee.dtdage.app.boot.starter.web.common.handler.ExceptionHandler;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * @author WFT
 * @since 2024/3/30
 */
@Slf4j
@Component
public class UnknownExceptionHandler implements ExceptionHandler<Class<Exception>> {

    @Override
    public <E extends Throwable> Response<?> resolve(E exception) {
        log.error(exception.getMessage(), exception);
        return new Response<>(HttpStatus.INTERNAL_SERVER_ERROR, exception.getMessage());
    }

    @Override
    public Class<Exception> getId() {
        return Exception.class;
    }

}