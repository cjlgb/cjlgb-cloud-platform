package io.gitee.dtdage.app.boot.starter.web.common.exception.handler;

import io.gitee.dtdage.app.boot.starter.web.common.context.HttpStatus;
import io.gitee.dtdage.app.boot.starter.web.common.context.Response;
import io.gitee.dtdage.app.boot.starter.web.common.handler.ExceptionHandler;
import org.springframework.stereotype.Component;

import java.util.concurrent.TimeoutException;

/**
 * @author WFT
 * @since 2024/3/30
 */
@Component
public class TimeoutExceptionHandler implements ExceptionHandler<Class<TimeoutException>> {

    @Override
    public <E extends Throwable> Response<?> resolve(E exception) {
        return new Response<>(HttpStatus.REQUEST_TIMEOUT, exception.getMessage());
    }

    @Override
    public Class<TimeoutException> getId() {
        return TimeoutException.class;
    }

}
