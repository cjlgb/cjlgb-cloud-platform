package io.gitee.dtdage.app.boot.starter.web.common.handler;

import io.gitee.dtdage.app.boot.starter.web.common.context.Ignore;
import io.gitee.dtdage.app.boot.starter.web.common.context.HttpStatus;
import io.gitee.dtdage.app.boot.starter.web.common.context.Response;
import org.springframework.core.MethodParameter;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.method.support.HandlerMethodReturnValueHandler;
import org.springframework.web.method.support.ModelAndViewContainer;

import javax.servlet.http.HttpServletResponse;
import java.io.PrintWriter;

/**
 * 接口返回值处理器
 *
 * @author WFT
 * @since 2024/4/15
 */
public class ControllerResultHandler implements HandlerMethodReturnValueHandler {

    @Override
    public boolean supportsReturnType(MethodParameter parameter) {
        if (null != parameter.getMethodAnnotation(Ignore.class)) {
            return false;
        }
        Class<?> clazz = parameter.getContainingClass();
        return clazz.isAnnotationPresent(RestController.class)
                || clazz.isAnnotationPresent(ResponseBody.class)
                || null != parameter.getMethodAnnotation(ResponseBody.class);
    }

    @Override
    public void handleReturnValue(Object result, MethodParameter parameter, ModelAndViewContainer container, NativeWebRequest request) throws Exception {
        // 标识请求是否已经在该方法内完成处理
        container.setRequestHandled(true);
        //  打印返回值
        HttpServletResponse response = request.getNativeResponse(HttpServletResponse.class);
        if (null != response) {
            response.setContentType("application/json;charset=UTF-8");
            try (PrintWriter printWriter = response.getWriter()) {
                printWriter.print(new Response<>(HttpStatus.OK, null, result));
            }
        }
    }

}
