package io.gitee.dtdage.app.boot.starter.web.security.service;

import io.gitee.dtdage.app.boot.starter.common.BaseBean;
import io.gitee.dtdage.app.boot.starter.web.security.context.AccessToken;
import io.gitee.dtdage.app.boot.starter.web.security.context.Principal;
import io.gitee.dtdage.app.boot.starter.web.security.context.SecurityContext;
import io.gitee.dtdage.app.boot.starter.web.security.exception.AuthenticationException;

import java.io.Serializable;

/**
 * 访问令牌接口
 *
 * @author WFT
 * @since 2024/4/15
 */
public interface AccessTokenService extends BaseBean<String> {

    /**
     * 获取当前登录用户的主体信息
     *
     * @param context {@link SecurityContext} 权限上下文
     * @return {@link Principal} 用户主体信息
     * @throws AuthenticationException 登录超时的情况下,将抛出407异常
     */
    Principal getPrincipal(SecurityContext context) throws AuthenticationException;

    /**
     * 生成访问令牌
     *
     * @param principal {@link Principal} 用户主体信息
     * @param clientId  {@link Long} 客户端编号
     * @param <T>       {@link Principal}
     * @return {@link AccessToken}
     */
    <T extends Principal> AccessToken generate(T principal, Serializable clientId);

}
