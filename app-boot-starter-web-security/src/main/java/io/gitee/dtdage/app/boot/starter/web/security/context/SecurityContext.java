package io.gitee.dtdage.app.boot.starter.web.security.context;

import io.gitee.dtdage.app.boot.starter.web.security.exception.BaseSecurityException;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * 权限上下文对象
 *
 * @author WFT
 * @since 2024/4/15
 */
@Getter
@Setter
public class SecurityContext implements Serializable {

    /**
     * 客户端编号
     */
    private String clientId;

    /**
     * 客户端场景
     */
    private String scene;

    /**
     * 令牌生成规则
     */
    private String rule;

    /**
     * 访问令牌
     */
    private String token;

    /**
     * 用户主体
     */
    private Principal principal;

    /**
     * 权限异常
     */
    private BaseSecurityException exception;

}