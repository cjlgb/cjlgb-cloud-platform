package io.gitee.dtdage.app.boot.starter.web.security;

import io.gitee.dtdage.app.boot.starter.web.security.service.TokenStorageService;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * @author WFT
 * @since 2024/4/14
 */
@ComponentScan
@Configuration
public class SecurityAutoConfiguration {

    @Bean
    @ConditionalOnMissingBean(value = TokenStorageService.class)
    public TokenStorageService tokenStorageService() {
        return new TokenStorageService() {

        };
    }

}
