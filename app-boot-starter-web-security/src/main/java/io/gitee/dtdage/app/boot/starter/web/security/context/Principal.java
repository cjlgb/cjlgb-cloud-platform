package io.gitee.dtdage.app.boot.starter.web.security.context;

import io.gitee.dtdage.app.boot.starter.common.BaseEntity;
import io.gitee.dtdage.app.boot.starter.common.Terminal;

import java.util.Collection;

/**
 * 用户主体接口
 *
 * @author WFT
 * @since 2024/4/15
 */
public interface Principal extends BaseEntity<Long> {

    /**
     * 该字段用于区分用户类型，例如：平台管理员,平台商户,普通用户...
     *
     * @return {@link Terminal}
     */
    @SuppressWarnings("unused")
    Terminal getTerminalId();

    /**
     * 获取权限列表
     *
     * @return {@link Collection}
     */
    Collection<String> getAuthorities();

    /**
     * 设置权限列表
     *
     * @param authorities {@link Collection}
     */
    @SuppressWarnings("unused")
    void setAuthorities(Collection<String> authorities);

}