package io.gitee.dtdage.app.boot.starter.web.security.exception;

import io.gitee.dtdage.app.boot.starter.web.common.context.HttpStatus;

/**
 * 403 异常
 *
 * @author WFT
 * @since 2022/1/4
 */
public class AccessDeniedException extends BaseSecurityException {

    public AccessDeniedException() {
        this("You don't have permission to access on this server.");
    }

    /**
     * @noinspection WeakerAccess
     */
    public AccessDeniedException(String message) {
        super(message, HttpStatus.FORBIDDEN);
    }

}
