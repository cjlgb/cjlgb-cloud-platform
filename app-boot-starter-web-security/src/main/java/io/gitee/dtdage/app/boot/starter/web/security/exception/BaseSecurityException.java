package io.gitee.dtdage.app.boot.starter.web.security.exception;

import io.gitee.dtdage.app.boot.starter.web.common.exception.BusinessException;

/**
 * 权限异常父类
 *
 * @author WFT
 * @since 2024/4/15
 */
public class BaseSecurityException extends BusinessException {

    BaseSecurityException(String message, int code) {
        super(message, code);
    }

}