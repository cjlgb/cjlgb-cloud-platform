package io.gitee.dtdage.app.boot.starter.data.storage.context;

import io.gitee.dtdage.app.boot.starter.common.context.BeanFactory;
import io.gitee.dtdage.app.boot.starter.data.storage.enums.Supplier;
import io.gitee.dtdage.app.boot.starter.data.storage.service.StorageService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

/**
 * 对象存储工厂
 *
 * @author WFT
 * @since 2024/4/14
 */
@Component
@RequiredArgsConstructor
public class StorageFactory extends BeanFactory<Supplier, StorageService> {

    @Override
    protected IllegalArgumentException illegalArgumentException() {
        return new IllegalArgumentException("请联系开发者实现此对象存储接口");
    }

}