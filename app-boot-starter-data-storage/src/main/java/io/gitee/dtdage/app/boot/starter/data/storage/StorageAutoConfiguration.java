package io.gitee.dtdage.app.boot.starter.data.storage;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * @author WFT
 * @since 2024/4/8
 */
@Configuration
@ComponentScan
public class StorageAutoConfiguration {

}
